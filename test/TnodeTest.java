
import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author Jaanus
 */
public class TnodeTest {

   @Test (timeout=1000)
   public void testBuildFromRPN() { 
      String s = "1 2 +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(1,2)", r);
      s = "2 1 - 4 * 6 3 / +";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(6,3))", r);
   }

   @Test (timeout=1000)
   public void testBuild2() {
      String s = "512 1 - 4 * -61 3 / +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-61,3))", r);
      s = "5";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "5", r);
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol() {
      Tnode t = Tnode.buildFromRPN ("2 xx");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol2() {
      Tnode t = Tnode.buildFromRPN ("x");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol3() {
      Tnode t = Tnode.buildFromRPN ("2 1 + xx");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers() {
      Tnode root = Tnode.buildFromRPN ("2 3");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers2() {
      Tnode root = Tnode.buildFromRPN ("2 3 + 5");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers() {
      Tnode t = Tnode.buildFromRPN ("2 -");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers2() {
      Tnode t = Tnode.buildFromRPN ("2 5 + -");
   }

  @Test (expected=RuntimeException.class)
   public void testTooFewNumbers3() {
      Tnode t = Tnode.buildFromRPN ("+");
   }

   @Test (timeout=1000)
   public void testSwapRotDup() {
      String swap = "2 5 SWAP -";
      Tnode swapTnode = Tnode.buildFromRPN (swap);
      String swapString = swapTnode.toString();
      assertEquals ("Tree: " + swap, "-(5,2)", swapString);

      String dup = "3 DUP *";
      Tnode dupTnode = Tnode.buildFromRPN (dup);
      String dupString = dupTnode.toString();
      assertEquals ("Tree: " + dup, "*(3,3)", dupString);

      String rot = "2 5 9 ROT - +";
      Tnode rotTnode = Tnode.buildFromRPN (rot);
      String rotString = rotTnode.toString();
      assertEquals ("Tree: " + rot, "+(9,-(2,5))", rotString);

      String rotSwap = "2 5 9 ROT + SWAP -";
      Tnode rotSwapTnode = Tnode.buildFromRPN (rotSwap);
      String rotSwapString = rotSwapTnode.toString();
      assertEquals ("Tree: " + rotSwap, "-(+(9,2),5)", rotSwapString);

      String dupRot = "2 5 DUP ROT - + DUP *";
      Tnode dupRotTnode = Tnode.buildFromRPN (dupRot);
      String dupRotString = dupRotTnode.toString();
      assertEquals ("Tree: " + dupRot, "*(+(5,-(2,5)),+(5,-(2,5)))", dupRotString);

      String swapRotDup = "-3 -5 -7 ROT - SWAP DUP * +";
      Tnode swapRotDupTnode = Tnode.buildFromRPN (swapRotDup);
      String swapRotDupString = swapRotDupTnode.toString();
      assertEquals ("Tree: " + swapRotDup, "+(-(-7,-3),*(-5,-5))", swapRotDupString);
   }
}

