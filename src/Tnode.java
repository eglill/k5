import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode() {
      this.name = null;
      this.firstChild = null;
      this.nextSibling = null;
   }

   public String getName(){
      return this.name;
   }

   public Tnode getFirstChild(){
      return this.firstChild;
   }

   public Tnode getNextSibling(){
      return this.nextSibling;
   }

   @Override
   public Tnode clone() {
      Tnode tNode = new Tnode();
      tNode.name = this.getName();
      tNode.firstChild = this.getFirstChild();
      tNode.nextSibling = this.getNextSibling();
      return tNode;
   }

   @Override
   public String toString() {
      return buildString();
   }

   public static Tnode buildFromRPN (String pol) {
      Stack<Tnode> stack = new Stack<>();
      Stack<String> elements;
      try {
         elements = errorCheck(pol);
         //If all good, returns stack. If not, throws error.
         // If "DUP" is after operator, then it will be pushed into stack.
      } catch (RuntimeException e) {
         throw new RuntimeException(e.getMessage());
      }

      boolean onlyOperatorsLeft;
      int numbersInRow;
      Tnode tnode;

      for (int i = 0; i < elements.size(); i++) {
         onlyOperatorsLeft = checkEnding(elements, i); //Checks if only operators left in stack.
         numbersInRow = numbersInRow(elements, i); //Checks how many numbers in row.

         tnode = new Tnode();
         tnode.name = elements.get(i);
         stack.push(tnode);
         if (!caseDup(stack)) { //If element is "DUP", it is popped and next element will be its own nextSibling.
            addChild(stack);
            addSibling(stack, numbersInRow, onlyOperatorsLeft);
         }
      }
      return stack.pop();
   }

   private static boolean checkEnding(Stack<String> elements, int i) {
      boolean allOperators = true;
      for (int j = i; j < elements.size(); j++) {
         if (isNumber(elements.get(j))) {
            allOperators = false;
         }
      }
      return allOperators;
   }

   private static void addChild(Stack<Tnode> stack) {
      Tnode parent = stack.pop();
      if (!isNumber(parent) && stack.size() > 0) {
         parent.firstChild = stack.pop();
      }
      stack.push(parent);
   }

   private static void addSibling(Stack<Tnode> stack, int numbersInRow, boolean onlyOperatorsLeft) {
      if (stack.size() > 1) {
         if (numbersInRow == 1 || onlyOperatorsLeft) {
            Tnode sibling = stack.pop();
            Tnode s = stack.lastElement();
            s.nextSibling = sibling;
         }
      }
   }

   private static boolean caseDup(Stack<Tnode> stack) {
      if (stack.peek().getName().equalsIgnoreCase("dup")) {
         stack.pop();
         Tnode sibling = stack.pop();
         sibling.nextSibling = sibling.clone();
         stack.push(sibling);
         return true;
      }
      return false;
   }

   private static boolean isNumber(Tnode n) {
      try {
         Integer.parseInt(n.getName());
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }

   private static boolean isNumber(String n) {
      try {
         Integer.parseInt(n);
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }

   private static int numbersInRow(Stack<String> elements, int i) {
      int count = 0;
      for (int j = i; j < elements.size(); j++) {
         if (isNumber(elements.get(j))) {
            count++;
         } else return count;
      }
      return count;
   }

   private static Stack<String> processInput(String input) {
      String[] elements = input.split(" ");
      Stack<String> stack = new Stack<>();
      for (String s : elements) {
         swapRotDup(s, stack);
      }
      return stack;
   }

   private static void swapRotDup(String element, Stack<String> stack) {
      String second;
      String first;
      String third;
      switch (element.toUpperCase()) {
         case "SWAP":
            if (stack.size() < 2) {
               throw new IllegalStateException("Can not perform SWAP with les than two elements in stack: "  + stack);
            } else {
               first = stack.pop();
               second = stack.pop();
               stack.push(first);
               stack.push(second);
            }
            return;
         case "ROT":
            if (stack.size() < 3) {
               throw new IllegalStateException("Can not perform ROT with les than three elements in stack: "  + stack);
            } else {
               first = stack.pop();
               second = stack.pop();
               third = stack.pop();
               stack.push(first);
               stack.push(third);
               stack.push(second);
            }
            return;
         case "DUP":
            if (stack.size() < 1) {
               throw new IllegalStateException("Can not perform DUP with les than one element in stack: "  + stack);
            } else {
               if (isNumber(stack.peek())) {
                  first = stack.pop();
                  stack.push(first);
                  stack.push(first);
               } else {
                  stack.push(element);
               }
            }
            return;
         default:
            stack.push(element);
      }
   }

   private String buildString() {
//      https://github.com/egaia/AlgorithmAndDataStructure/blob/master/home5/src/Node.java

      StringBuilder b = new StringBuilder();

      b.append(this.getName());

      if(getFirstChild() != null){
         b.append("(");
         b.append(getFirstChild().buildString());
         b.append(")");
      }

      if(getNextSibling() != null){
         b.append(",");
         b.append(getNextSibling().buildString());
      }
      return b.toString();
   }

   private static Stack<String> errorCheck(String s){
      Stack<String> checked;
      try {
         checked = processInput(s.toUpperCase());
      } catch (RuntimeException e) {
         throw new RuntimeException(e.getMessage() + ". Input: " + s);
      }

      if(s.length() == 0) {
         throw new RuntimeException("Tree is empty: " + s);
      }
      try {
         checkIllegalSymbol(checked);
      } catch (RuntimeException e) {
         throw new RuntimeException(e.getMessage() + s);
      }
      try {
         checkNumbersCount(checked);
      } catch (RuntimeException e) {
         throw new RuntimeException(e.getMessage() + s);
      }
      return checked;
   }

   private static void checkNumbersCount(Stack<String> checked) {
      int numbersCount = 0;
      int operatorCount = 0;
      for (String s : checked) {
         if (s.equalsIgnoreCase("DUP")) {
            numbersCount = numbersCount * 2;
            operatorCount = operatorCount * 2;
         } else if (isNumber(s)) {
            numbersCount++;
         } else {
            operatorCount++;
         }
      }
      if (numbersCount > operatorCount + 1) {
         throw new RuntimeException("Too many numbers: ");
      } else if (numbersCount < operatorCount + 1) {
         throw new RuntimeException("Too few numbers: ");
      }
   }

   private static void checkIllegalSymbol(Stack<String> checked) {
      String[] cases = {"SWAP", "ROT", "DUP", " ", "+", "-", "*", "/"};

      boolean condition = false;
      for (String element : checked) {
         if (isNumber(element)) {
            condition = true;
         } else {
            for (String c : cases) {
               if (c.equals(element)) {
                  condition = true;
                  break;
               }
            }
         }
         if (!condition) {
            throw new RuntimeException(element + "is illegal:  ");
         }
         condition = false;
      }
   }

   public static void main (String[] param) {
      String rpn = "-3 -5 -7 ROT - SWAP DUP * +";

      Tnode res = buildFromRPN (rpn);
      System.out.println(res.toString());

   }
}